INTRODUCTION
------------

This module serves to bring bulk update in Nodeaccess module. 
One of the limitations of Nodeaccess is that it only works on single node 
at a time. 
This is an implementation of Views bulk Operations to bring bulk operations 
in Nodeaccess.


INSTALLATION
-----------------

1. Create a View with a page or block display.
2. Add a "Views bulk operations" field (global), available on
   all entity types.
3. Configure the field by selecting at least Nodeaccess Bulk operation.
4. Go to the View page. VBO functionality should be present.


REQUIREMENTS
------------

The module is dependant on VBO and Nodeaccess


CONFIGURATION
--------------
No additional configuration needed
