<?php

namespace Drupal\nodeaccess_bulk\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;

/**
 * Nodeaccess bulk operation.
 *
 * @Action(
 *   id = "nodeaccess_bulk",
 *   label = @Translation("Nodeaccess Bulk Update"),
 *   type = "",
 *   confirm = TRUE,
 * )
 */
class NodeaccessBulkAction extends ViewsBulkOperationsActionBase implements
    ViewsBulkOperationsPreconfigurationInterface,
    PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $values = $this->configuration["nodeaccess_bulk_config"];

    $nid = $entity->id();
    $grants = [];
    $node = Node::load($nid);

    foreach (["uid", "rid"] as $type) {
      $realm = "nodeaccess_" . $type;
      if (isset($values[$type]) && is_array($values[$type])) {
        foreach ($values[$type] as $gid => $line) {
          $grant = [
            "gid" => $gid,
            "realm" => $realm,
            "grant_view" => empty($line["grant_view"])
            ? 0
            : $line["grant_view"],
            "grant_update" => empty($line["grant_update"])
            ? 0
            : $line["grant_update"],
            "grant_delete" => empty($line["grant_delete"])
            ? 0
            : $line["grant_delete"],
          ];
          if ($grant["grant_view"] ||$grant["grant_update"] ||$grant["grant_delete"]) {
            $grants[] = $grant;
          }
        }
      }
    }
    // Save role and user grants to our own table.
    \Drupal::database()
      ->delete("nodeaccess")
      ->condition("nid", $nid)
      ->execute();
    foreach ($grants as $grant) {
      $id = db_insert("nodeaccess")
        ->fields([
          "nid" => $nid,
          "gid" => $grant["gid"],
          "realm" => $grant["realm"],
          "grant_view" => $grant["grant_view"],
          "grant_update" => $grant["grant_update"],
          "grant_delete" => $grant["grant_delete"],
        ])
        ->execute();
    }
    \Drupal::entityTypeManager()
      ->getAccessControlHandler("node")
      ->writeGrants($node);
    $tags = ["node:" . $node->id()];
    Cache::invalidateTags($tags);

    $this->messenger()->addMessage($this->t("Grants saved."));
    $this->messenger()->addMessage($entity->label() . "-" . "Id: " . $entity->id());
  }

  /**
   * Configuration form builder.
   *
   * @param array $form
   *   Form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The configuration form.
   */
  public function buildConfigurationForm(
        array $form,
        FormStateInterface $form_state
    ) {
    $form_values = $form_state->getValues();
    $settings = \Drupal::configFactory()->get("nodeaccess.settings");
    $role_alias = $settings->get("role_alias");
    $role_map = $settings->get("role_map");
    $allowed_roles = [];
    $user = \Drupal::currentUser();
    $allowed_grants = $settings->get("grants");
    foreach ($role_alias as $id => $role) {
      if ($role["allow"]) {
        $allowed_roles[] = $id;
      }
    }

    // Perform search.
    if (isset($form_values["keys"])) {
      $uids = [];
      $sql =
                "SELECT uid, name FROM {users_field_data} WHERE uid in (:uids[])";
      if (isset($form_values["uid"]) && is_array($form_values["uid"])) {
        $uids = array_keys($form_values["uid"]);
      }
      if (!in_array($form_values["keys"], $uids)) {
        array_push($uids, $form_values["keys"]);
      }
      $params = [":uids[]" => $uids];
      $result = db_query($sql, $params);
      foreach ($result as $account) {
        $form_values["uid"][$account->uid] = [
          "name" => $account->name,
          "keep" => 0,
        ];
      }
    }

    $form_values["rid"] = isset($form_values["rid"])
            ? $form_values["rid"]
            : [];
    $form_values["uid"] = isset($form_values["uid"])
            ? $form_values["uid"]
            : [];
    $roles = $form_values["rid"];
    $users = $form_values["uid"];

    // If $preserve is TRUE, the fields the user is not allowed to view or
    // edit are included in the form as hidden fields to preserve them.
    $preserve = $settings->get("preserve");
    // Roles table.
    if (count($allowed_roles)) {
      $header = [];
      $header[] = $this->t("Role");
      if ($allowed_grants["view"]) {
        $header[] = $this->t("View");
      }
      if ($allowed_grants["edit"]) {
        $header[] = $this->t("Edit");
      }
      if ($allowed_grants["delete"]) {
        $header[] = $this->t("Delete");
      }
      $form["rid"] = [
        "#type" => "table",
        "#header" => $header,
        "#tree" => TRUE,
      ];
      foreach ($allowed_roles as $id) {
        $rid = $role_map[$id];
        $form["rid"][$rid]["name"] = [
          "#markup" => $role_alias[$id]["alias"],
        ];
        if ($allowed_grants["view"]) {
          $form["rid"][$rid]["grant_view"] = [
            "#type" => "checkbox",
            "#default_value" => "",
          ];
        }
        if ($allowed_grants["edit"]) {
          $form["rid"][$rid]["grant_update"] = [
            "#type" => "checkbox",
            "#default_value" => "",
          ];
        }
        if ($allowed_grants["delete"]) {
          $form["rid"][$rid]["grant_delete"] = [
            "#type" => "checkbox",
            "#default_value" => "",
          ];
        }
      }
    }

    // Autocomplete returns errors if users don't have access to profiles.
    if ($user->hasPermission("access user profiles")) {
      $form["keys"] = [
        "#type" => "entity_autocomplete",
        "#default_value" => isset($form_values["keys"])
        ? $form_values["keys"]
        : "",
        "#size" => 40,
        "#target_type" => "user",
        "#title" => $this->t("Enter names to search for users"),
      ];
    }
    else {
      $form["keys"] = [
        "#type" => "textfield",
        "#default_value" => isset($form_values["keys"])
        ? $form_values["keys"]
        : "",
        "#size" => 40,
      ];
    }
    $form["keys"]["#prefix"] = '<p><div class="container-inline">';
    $form["search"] = [
      "#type" => "submit",
      "#value" => $this->t("Search"),
      "#submit" => [[$this, "searchUser"]],
      "#suffix" => "</div></p>",
    ];
    // Users table.
    if (count($users)) {
      $header = [];
      $header[] = $this->t("User");
      $header[] = $this->t("Keep?");
      if ($allowed_grants["view"]) {
        $header[] = $this->t("View");
      }
      if ($allowed_grants["edit"]) {
        $header[] = $this->t("Edit");
      }
      if ($allowed_grants["delete"]) {
        $header[] = $this->t("Delete");
      }
      $form["uid"] = [
        "#type" => "table",
        "#header" => $header,
      ];
      foreach ($users as $uid => $account) {
        $form["uid"][$uid]["name"] = [
          "#markup" => $account["name"],
        ];
        $form["uid"][$uid]["keep"] = [
          "#type" => "checkbox",
          "#default_value" => empty($account["grant_keep"])
          ? 1
          : $account["grant_keep"],
        ];
        if ($allowed_grants["view"]) {
          $form["uid"][$uid]["grant_view"] = [
            "#type" => "checkbox",
            "#default_value" => empty($account["grant_view"])
            ? ""
            : $account["grant_view"],
          ];
        }
        if ($allowed_grants["edit"]) {
          $form["uid"][$uid]["grant_update"] = [
            "#type" => "checkbox",
            "#default_value" => empty($account["grant_update"])
            ? ""
            : $account["grant_update"],
          ];
        }
        if ($allowed_grants["delete"]) {
          $form["uid"][$uid]["grant_delete"] = [
            "#type" => "checkbox",
            "#default_value" => empty($account["grant_delete"])
            ? ""
            : $account["grant_delete"],
          ];
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(
        array $form,
        array $values,
        FormStateInterface $form_state
    ) {
    $form["nodeaccess_bulk_config"] = [
      "#title" => $this->t("Add cofirmation step"),
      "#type" => "checkbox",
      "#default_value" => isset($values["nodeaccess_bulk_config"])
      ? $values["nodeaccess_bulk_config"]
      : TRUE,
    ];
    return $form;
  }

  /**
   * Submit the config form.
   *
   * @param array $form
   *   Form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {
    $this->configuration["nodeaccess_bulk_config"] = $form_state->getValues();
  }

  /**
   * {@inheritdoc}
   */
  public function access(
        $object,
        AccountInterface $account = NULL,
        $return_as_object = FALSE
    ) {
    if ($object->getEntityType() === "node") {
      $access = $object
        ->access("update", $account, TRUE)
        ->andIf($object->status->access("edit", $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }
    return TRUE;
  }

  /**
   * Helper function to search usernames.
   */
  public function searchUser(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state->setRebuild();
    $form_state->setStorage($values);
  }

}
